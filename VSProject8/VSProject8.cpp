﻿#include <iostream>

class Animal 
{
public:
    virtual void Voice()
    {
        std::cout << "";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meow";
    }
};
class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof";
    }
};
class Cow : public Animal
{
    void Voice() override
    {
        std::cout << "Moo";
    }
};
int main()
{
    Dog Sharik;
    Cat Matroskin;
    Cow Murka;

    Animal* array[3];
    array[0] = &Sharik;
    array[1] = &Matroskin;
    array[2] = &Murka;

    for (int i = 0; i < 3; i++)
    {
        array[i]->Voice();
    }
}